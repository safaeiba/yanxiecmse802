import sys
import pandas as pd
import pytest
from multi_xvg import multi_xvg
from multi_colvar import multi_colvar
from association_plot import association_plot 
from association_num import association_num

def test_association_plot():
    dc=multi_colvar(4)
    dc=association_num(dc)
    dxvg=multi_xvg(4)
    assert association_plot(dc,dxvg)
    
    
def test_bad_association_plot():
    dc=multi_colvar(2)
    dc=association_num(dc)
    dxvg=multi_xvg(3)
    with pytest.raises(ValueError) as excinfo:
        association_plot(dc,dxvg)
    assert "Length of inputs does't match." == str(excinfo.value)