from read_colvar import read_colvar

def multi_colvar(n=1):
    '''
    This function is to read multiple colvar files and put them into one dataframe.
    
    Parameters:
    
    n: number of colvar files you want to load. This is dependent on how many data files you have. n cannot beyond the total number of files
    
    Returns:
    
    dc: pandas dataframe. Every page is from an independent simulation. The first column of dc is time, and the following columns are all distances with each residue. 
    
    '''
    dc=[]
    for i in range(1,n+1):
        dd=read_colvar('Data/COLVAR'+str(i),verbose=True)
        dd.time /=1000
        dc.append(dd)
    return dc

