import pandas as pd
import six


def read_colvar(fname, verbose=False):
    '''
    Load colvar file.
    
    Parameters:
    
    fname: Str, the name of colvar file. 
    
    verbose: False or True. False hides the loaded file's name, and True shows the name.
    
    Returns:
    
    df: return a dataframe that contains the simulation time and distances
    
    '''
    if verbose: print(fname)
    f = open(fname) if isinstance(fname, six.string_types) else fname
    with f:
        line = f.readline()
        assert line[:2] == '#!', 'Missing or incorrect header'
        columns = line.split()[2:]
        f.seek(0)
        df = pd.read_csv(f, delim_whitespace=True, comment="#", 
                         skipinitialspace=True,
                         skiprows=1, header=None)
    df.columns = columns
    return df 

