import pandas as pd
import pytest
from multi_xvg import multi_xvg

def test_good_multi_xvg():
    #test a good set of colvar files
    import os
    print(os.getcwd())
    assert len(multi_xvg(3))==3
     
def test_bad_multi_xvg():
    # test a bad load example of multiple colvars
    assert len(multi_xvg(5))!=4