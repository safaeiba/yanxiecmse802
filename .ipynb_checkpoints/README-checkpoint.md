# YanXieCMSE802

Hi, this is my repository. 


Let me briefly navigate you through this repository. Here is the outline:

**PROJECT_PROPOSAL:**
    I put mu original project proposal inside this folder.

**REPORTS:**, 
    All the weekly reports that we need to write based on our own research are included in this folder. There are four of them and I didn't put those that only need us to learn and fill the google forms. 

**FINAL_REPORT:**
   In this folder, I put the final report notebook inside which is a comprehensive summary of this project. In order to understand the purpose and status of this project, please take a look at the final report.
   
**SOFTWARE:**
    This part is the most important section of my project. All python functions and the corresponding test functions, the example of running these softwares and the dataset are included here. 
    **The zeroth step** before you start running the code is to install all the required packages that are included in **requirements.txt** by executing `> pip install -r requirements.text` under the main folder `yanxiecmse802`. 
    **The first step** to run the test functions under `SOFTWARE` folder with the command `> pytest`. If this works fine, then go ahead.
    **The second step** to run the `Example.ipynb`. All the functions are called and applyed to solve my research problems. 
    
In the end, I hope the strucutre of my project is clear to you and you didn't encounter any difficulty with debugging.